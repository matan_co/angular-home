import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class UsersService {
  http:Http;
  getusers(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/slim/users');
  }
  constructor(http:Http) { 
    this.http = http;
  }
}