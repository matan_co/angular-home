import { HttpModule } from '@angular/http';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';


@NgModule({
  declarations: [
    AppComponent,
       UsersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
       UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }